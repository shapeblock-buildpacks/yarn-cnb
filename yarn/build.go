package node

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/paketo-buildpacks/packit"
	"github.com/paketo-buildpacks/packit/fs"
	"gopkg.in/yaml.v2"
)

func computeChecksum(fileName string) string {
	f, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}

	return hex.EncodeToString(h.Sum(nil))
}

func Build() packit.BuildFunc {
	return func(context packit.BuildContext) (packit.BuildResult, error) {

		yamlFile, err := ioutil.ReadFile(filepath.Join(context.WorkingDir, ".platform.app.yaml"))
		if err != nil {
			return packit.BuildResult{}, err
		}
		var config PlatformConfig

		err = yaml.Unmarshal(yamlFile, &config)
		if err != nil {
			return packit.BuildResult{}, err
		}

		yarnLayer, err := context.Layers.Get("yarn")
		if err != nil {
			return packit.BuildResult{}, err
		}

		// if checksum is different, then run these, else, just symlink and return
		// Todo: apply checksum for nodejs.dependencies and nodejs version as well.
		yarnLock := filepath.Join(context.WorkingDir, "yarn.lock")

		workingDirNodeModules := filepath.Join(context.WorkingDir, "node_modules")
		yarnLayerNodeModules := filepath.Join(yarnLayer.Path, "node_modules")

		sum := computeChecksum(yarnLock)
		prevSHA, ok := yarnLayer.Metadata["cache_sha"].(string)
		if (ok && sum != prevSHA) || !ok {

			yarnLayer, err = yarnLayer.Reset()
			if err != nil {
				return packit.BuildResult{}, err
			}

			cacheDir := filepath.Join(yarnLayer.Path, "npm-cache")

			for library, version := range config.Dependencies["nodejs"] {
				dependency := fmt.Sprintf("%s@%s", library, version)
				fmt.Printf("Dependency -> %s\n", dependency)
				cmd := exec.Command("npm", "install", "--no-package-lock", "--no-save", dependency, "--unsafe-perm", "--cache", cacheDir)
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr
				if err = cmd.Run(); err != nil {
					return packit.BuildResult{}, err
				}
			}

			yarnBin := filepath.Join(yarnLayer.Path, "yarn_node_modules")
			err = fs.Move(workingDirNodeModules, yarnBin)
			if err != nil {
				fmt.Errorf("failed to move node_modules directory to yarn bin: %w", err)
				return packit.BuildResult{}, err
			}

			entry := context.Plan.Entries[0]
			buildHook, _ := entry.Metadata["build"].(string)

			buildScript := filepath.Join(yarnLayer.Path, "build")
			err = ioutil.WriteFile(buildScript, []byte(buildHook), 0644)
			if err != nil {
				return packit.BuildResult{}, err
			}

			binPath := strings.Join([]string{os.Getenv("PATH"), filepath.Join(yarnBin, ".bin")}, string(os.PathListSeparator))
			err = os.Setenv("PATH", binPath)
			if err != nil {
				return packit.BuildResult{}, err
			}
			fmt.Printf("Running build hook ->\n")
			cmd := exec.Command("bash", buildScript)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err = cmd.Run(); err != nil {
				return packit.BuildResult{}, err
			}

			yarnLayer.Metadata = map[string]interface{}{
				"cache_sha": sum,
			}

			// move node_modules to yarnLayer Path
			fmt.Printf("Moving working dir node_modules to cache. ->\n")
			err = fs.Move(workingDirNodeModules, yarnLayerNodeModules)
			if err != nil {
				fmt.Errorf("failed to move node_modules directory to layer: %w", err)
				return packit.BuildResult{}, err
			}

		} else {
			fmt.Printf("Resuing cache layer\n")
			err := os.RemoveAll(workingDirNodeModules)
			if err != nil {
				return packit.BuildResult{}, err
			}
		}

		fmt.Printf("Symlinking ->\n")
		err = os.Symlink(yarnLayerNodeModules, workingDirNodeModules)
		if err != nil {
			fmt.Errorf("failed to symlink node_modules into working directory: %w", err)
			return packit.BuildResult{}, err
		}

		yarnLayer.Build = true
		yarnLayer.Cache = true

		// yarnLayer.ProcessLaunchEnv["web"].Append("PATH", filepath.Join(yarnLayer.Path, "node_modules", ".bin"), string(os.PathListSeparator))

		buildResult := packit.BuildResult{
			Plan: context.Plan,
			Layers: []packit.Layer{
				yarnLayer,
			},
		}
		if context.Plan.Entries != nil {
			entry := context.Plan.Entries[0]
			startCmd, _ := entry.Metadata["start"].(string)
			buildResult.Launch = packit.LaunchMetadata{
				Processes: []packit.Process{
					{
						Type:    "web",
						Command: startCmd,
					},
				},
			}
		}
		return buildResult, nil
	}
}
