module gitlab.com/shapeblock-buildpacks/yarn-cnb

go 1.16

require (
	github.com/otiai10/copy v1.6.0 // indirect
	github.com/paketo-buildpacks/packit v0.13.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
