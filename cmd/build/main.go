package main

import (
	yarn "gitlab.com/shapeblock-buildpacks/yarn-cnb/yarn"

	"github.com/paketo-buildpacks/packit"
)

func main() {
	packit.Build(yarn.Build())
}
